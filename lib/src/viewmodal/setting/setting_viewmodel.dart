import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:read_paper/src/model/detail_voice_read.dart';

import '../../../core/helper/network.dart';

class SettingViewModel extends ChangeNotifier {
  SettingViewModel() {}
  int statusCode = 0;

  Future<DetailVoiceRead?> getDetailVoidRead() async {
    String base_url = "/appconfigvoice/get-detail";
    var response = await ApiProvider().get(base_url);
    if (response != null) {
      return DetailVoiceRead.fromJson(response);
    }
    return null;
  }

  Future<dynamic> setDetailVoidRead(Map<String, dynamic> request) async {
    String base_url = "/appconfigvoice/insert";
    Response response = await ApiProvider().post(base_url,request);
    if (response.statusCode == 200) {
      return statusCode;
    }
  }
}
