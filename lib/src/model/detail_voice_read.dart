
class DetailVoiceRead {

  int? id;
  String? giongDoc;
  String? tocDo;
  String? caoDo;
  String? vanBanChayThu;
  String? username;

  DetailVoiceRead({
    this.id,
    this.giongDoc,
    this.tocDo,
    this.caoDo,
    this.vanBanChayThu,
    this.username,
  });
  DetailVoiceRead.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    giongDoc = json['giongDoc']?.toString();
    tocDo = json['tocDo']?.toString();
    caoDo = json['caoDo']?.toString();
    vanBanChayThu = json['vanBanChayThu']?.toString();
    username = json['username']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['giongDoc'] = giongDoc;
    data['tocDo'] = tocDo;
    data['caoDo'] = caoDo;
    data['vanBanChayThu'] = vanBanChayThu;
    data['username'] = username;
    return data;
  }
}

class ExtraVoice {

  String? key;
  String? locale;
  String? name;

  ExtraVoice({
    this.key,
    this.locale,
    this.name,
  });
  ExtraVoice.fromJson(Map<String, dynamic> json) {
    key = json['key']?.toString();
    locale = json['locale']?.toString();
    name = json['name']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['key'] = key;
    data['locale'] = locale;
    data['name'] = name;
    return data;
  }
}
